From: Gitolite <devnull@kernel.org>
Subject: post-receive: pub/scm/linux/kernel/git/netdev/net-next
Date: Sat, 31 Oct 2020 23:37:39 -0000
Message-Id: <160436170720.22617.5676697384317144135@gitolite.kernel.org>
MIME-Version: 1.0
Content-Type: text/plain; charset="utf-8"
Content-Transfer-Encoding: 7bit

---
service: git-receive-pack
user: kuba
repo: pub/scm/linux/kernel/git/netdev/net-next
changes:
  - ref: refs/heads/master
    old: c24672cf59b6f7da102833569fa8455132b2b8c2
    new: 1c470b53ece5833193a5b28b6e89cb1531c1de8c
